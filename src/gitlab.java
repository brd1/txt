import java.util.ArrayList;
        import java.util.List;
        import java.util.Scanner;
        import java.sql.Connection;
        import java.sql.DriverManager;
        import java.sql.ResultSet;
        import java.sql.SQLException;
        import java.sql.Statement;
        import java.sql.ResultSetMetaData;
public class gitlab {
    private static String dbURL = "jdbc:derby://localhost:1527/myDB;create=true";
    private static String tableName = "restaurants";
    // jdbc Connection77777
    //5555
    private static Connection conn = null;
    private static Statement stmt = null;
    public static void main(String[] args){


        createConnection();
        insertRestaurants(5, "LaVals", "Berkeley");
        selectRestaurants();
        shutdown();
    }


    private static void createConnection()
    {
        int a=0;
        int b=4;
        try
        {

            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            //Get a connection
            conn = DriverManager.getConnection(dbURL);
        }
        catch (Exception except)
        {
            except.printStackTrace();
        }
    }

    private static void insertRestaurants(int id, String restName, String cityName)
    {
        try
        {
            stmt = conn.createStatement();
            stmt.execute("insert into " + tableName + " values (" +
                    id + ",'" + restName + "','" + cityName +"')");
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {

            if(sqlExcept.getMessage().equals("Table/View '" + tableName.toUpperCase() + "' does not exist.")){
                try {
                    stmt.execute("CREATE TABLE " + tableName + "(id int,restName varchar(255),cityName varchar(255))");
                    insertRestaurants(id , restName, cityName);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }else{
                sqlExcept.printStackTrace();
            }
        }



    }

    private static void selectRestaurants()
    {
        try
        {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("select * from " + tableName);
            ResultSetMetaData rsmd = results.getMetaData();
            int numberCols = rsmd.getColumnCount();
            for (int i=1; i<=numberCols; i++)
            {
                //print Column Names
                System.out.print(rsmd.getColumnLabel(i)+"\t\t");
            }

            System.out.println("\n-------------------------------------------------");

            while(results.next())
            {
                int id = results.getInt(1);
                String restName = results.getString(2);
                String cityName = results.getString(3);
                System.out.println(id + "\t\t" + restName + "\t\t" + cityName);
            }
            results.close();
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
    }

    private static void shutdown()
    {
        try
        {
            if (stmt != null)
            {
                stmt.close();
            }
            if (conn != null)
            {
                DriverManager.getConnection(dbURL + ";shutdown=true");
                conn.close();
            }
        }
        catch (SQLException sqlExcept)
        {

        }



      /*  String a = null;
        int b = 0;
        Scanner tst=new Scanner(System.in);
        System.out.println("please inter the tedad");
        int tstt=(tst.nextInt());
        List<String> list = new ArrayList<String>();
        List<Integer> list2 = new ArrayList<Integer>();
        String[] t=new String[tstt];
        for(int k=0;k<t.length;k++){
            System.out.println("please inter the name");
            a=(tst.next());
           list.add(a);
            System.out.println("please inter the number");
            b=tst.nextInt();
            list2.add(b);
        }
        System.out.println("please inter the esm");
        String f=tst.next();
       for (int i=0;i<list.size();i++){
            if (f.contains(list.get(i))){
                    System.out.println("number is"+list2.get(i));
            }
       }
        System.out.println("not found");*/
    }
}
